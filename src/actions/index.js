import * as type from './../constants/ActionType';

export const pageAboutMe = () => {
    return {
        type: type.PAGE_ABOUT_ME
    };
};

export const pageKnowledge = () => {
    return {
        type: type.PAGE_KNOWLEDGE
    };
};

export const pageContact = () => {
    return {
        type: type.PAGE_CONTACT
    };
};