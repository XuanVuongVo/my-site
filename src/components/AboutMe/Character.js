import React, { Component } from 'react';
import { Layout, Menu, Icon, Button, Badge, Row, Col, Avatar, Input } from 'antd';
import '../../style/character.css';
import Item from './Item';
const Send = Input.Search;

class Character extends Component {
    render() {
        return (
            <div className="character-content">
                <div className="title">
                    <div className="line-bar"></div>
                    <h2>
                        CHARACTER
                    </h2>
                </div>
                <div className="detail">
                    <Item type="api" name="Honest"/>
                    <Item type="api" name="Friendly"/>
                    <Item type="api" name="Enthusiatic"/>
                    <Item type="api" name="Studious"/>
                </div>
            </div>
        );
    }
}
  
export default Character;
  