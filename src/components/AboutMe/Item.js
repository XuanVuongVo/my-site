import React, { Component } from 'react';
import { Icon } from 'antd';
import '../../style/character.css';

class Character extends Component {
    render() {
        return (
            <div className="item">
                <Icon type={this.props.type}/>
                {this.props.name}
            </div>
        );
    }
}
  
export default Character;
  