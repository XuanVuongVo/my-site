import React, { Component } from 'react';
import '../../style/target.css';
import Item from './Item';

class Target extends Component {
    render() {
        return (
            <div className="target-content">
                <div className="title">
                    <div className="line-bar"></div>
                    <h2>
                        TARGET
                    </h2>
                </div>
                <div className="detail">
                    <Item type="pushpin-o" name="To be a good staff at a company."/>
                    <Item type="pushpin-o" name="Try to learning as much as posible."/>
                    <Item type="pushpin-o" name="Looking for a good opportunities."/>
                    <Item type="pushpin-o" name="Learning more hard and soft skills to improve myself."/>
                </div>
            </div>
        );
    }
}
  
export default Target;
  