import React, { Component } from 'react';
import '../style/header.css';
import avatar from '../image/avatar1.jpg';
class Header extends Component {
    render() {
        return (
            <section className="sec-header">
                <div className="avatar">
                    <img src={avatar}/>
                </div>
                <div className="name">
                    VO XUAN VUONG
                </div>
                <div className="description">
                    Learn more to become a web developer in near future!
                </div>
            </section>
        );
    }
}
  
export default Header;
  