import React, { Component } from 'react';
import '../style/about-me.css';
import DetailSkill from './Knowledge/DetailSkill';
import Projects from './Knowledge/Projects';

class Knowledge extends Component {
    render() {
        return (
            <section className="sec-skill">
               <DetailSkill/>
               <Projects/>
            </section>
        );
    }
}
  
export default Knowledge;
  