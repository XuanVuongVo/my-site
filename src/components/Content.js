import React, { Component } from 'react';
import '../style/content.css';
import AboutMe from './AboutMe';
import Knowledge from './Knowledge';
import Contact from './Contact';
import NavBar from './NavBar';
import { connect } from 'react-redux';
import * as actions from './../actions/index';

class Content extends Component {
    render() {
        const {page} = this.props;
        console.log(page);
        if(page == 'PAGE_CONTACT') {
            return (
                <section className="sec-content">
                    <NavBar type2="1px solid #ffffff"/>
                    <Contact/>
                </section>
            );
        }
        if(page == 'PAGE_KNOWLEDGE') {
            return (
                <section className="sec-content">
                    <NavBar type3="1px solid #ffffff"/>
                    <Knowledge/>
                </section>
            );
        }
        else {
            return (
                <section className="sec-content">
                    <NavBar type1="1px solid #ffffff"/>
                    <AboutMe/>
                </section>
            );
        }
        
    }
}
var mapStateToProp = (state) => ({
    page: state.page
});
var mapDispatchToProp = (dispatch,props) => ({
});
export default connect(mapStateToProp,mapDispatchToProp)(Content);
  