import React, { Component } from 'react';
import '../style/contact.css';
import ContactItem from './Contact/ContactItem';

class Contact extends Component {
    render() {
        return (
            <section className="sec-contact">
               <div className="contact-content">
                    <div className="title">
                        <div className="line-bar"></div>
                        <h2>
                            CONTACT
                        </h2>
                    </div>
                    <div className="detail">
                        <ul>
                           <ContactItem type="mail" content="vuongvo1809@gmail.com"/>
                           <ContactItem type="phone" content="01629291336"/>
                           <ContactItem type="facebook" content="facebook.com/nvuong.vo"/> 
                           <ContactItem type="home" content="Dorm B, National University,HCM city"/>
                        </ul>
                    </div>
                </div>
            </section>
        );
    }
}
  
export default Contact;
  