import React, { Component } from 'react';
import 'antd/dist/antd.css';
import '../style/App.css';
import AppLayout from './Layout';

class App extends Component {
  render() {
    return (
      <div className="App">
        <AppLayout/>
      </div>
    );
  }
}

export default App;
