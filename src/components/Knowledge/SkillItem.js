import React, { Component } from 'react';
import '../../style/detail-skills.css';

class SkillItem extends Component {
    render() {
        return (
            <div className="item">
                <div className="item-left">
                    {this.props.name}
                </div>
                <div className="item-right">
                    <div className="level" style={{width: this.props.level}}></div>
                </div>
            </div>
        );
    }
}
  
export default SkillItem;
  