import React, { Component } from 'react';
import '../../style/detail-skills.css';
import SkillItem from './SkillItem';

class Skills extends Component {
    render() {
        return (
            <div className="skills-content">
                <div className="title">
                    <div className="line-bar"></div>
                    <h2>
                        SKILLS
                    </h2>
                </div>
                <div className="detail">
                    <div className="left"></div>
                    <div className="right">
                        <span>Beginner</span>
                        <span>Familiar</span>
                        <span>Proficient</span>
                        <span>Expert</span>
                    </div>
                    <SkillItem name="HTML,CSS" level="30%"/>
                    <SkillItem name="Javascript" level="20%"/>
                    <SkillItem name="MySQL" level="40%"/>
                    <SkillItem name="PHP" level="45%"/>
                    <SkillItem name="Analysis" level="30%"/>
                </div>
            </div>
        );
    }
}
  
export default Skills;
  