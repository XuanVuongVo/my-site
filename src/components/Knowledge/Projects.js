import React, { Component } from 'react';
import '../../style/projects.css';
import ProjectItem from './ProjectItem';

class Projects extends Component {
    render() {
        return (
            <div className="projects-content">
                <div className="title">
                    <div className="line-bar"></div>
                    <h2>
                        PROJECTS
                    </h2>
                </div>
                <div className="detail">
                    <ProjectItem role="Developer, Analyst" technology="Java, MySQL, SWT, JDBC" name="Cinema Management Project"/>
                    <ProjectItem role="Developer, Analyst" technology="HTML, CSS, Jquery, PHP, CodeInigter" name="Food Review Website"/>
                </div>
            </div>
        );
    }
}
  
export default Projects;
  