import React, { Component } from 'react';
import { Icon } from 'antd';
import '../../style/detail-skills.css';

class ProjectItem extends Component {
    render() {
        return (
            <div className="item">
                <div className="project-name">
                    <Icon type="desktop" />
                    <span>{this.props.name}</span>
                </div>
                <div className="project-detail">
                    <span>Role: {this.props.role}</span>
                    <span>Technology used: {this.props.technology}</span>
                </div>
            </div>
        );
    }
}
  
export default ProjectItem;
  