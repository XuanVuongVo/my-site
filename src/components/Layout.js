import React, { Component } from 'react';
import '../style/layout.css';
import Header from './Header';
import Content from './Content';

class appLayout extends Component {
    render() {
        return (
            <div className="default-layout">
                <Header/>
                <Content/>
            </div>
        );
    }
}
  
export default appLayout;
  