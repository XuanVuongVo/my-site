import React, { Component } from 'react';
import { Icon } from 'antd';
import '../style/navbar.css';
import { connect } from 'react-redux';
import * as actions from './../actions/index';

class NavBar extends Component {
    handleClickAboutMe = () => {
        this.props.pageAboutMe();
    }
    handleClickContact = () => {
        this.props.pageContact();
    }
    handleClickKnowledge = () => {
        this.props.pageKnowledge();
    }
    render() {
        return (
            <ul className="nav-bar">
                <li classNam="about" onClick={this.handleClickAboutMe} style={{borderBottom: this.props.type1}}>
                    <Icon type="user" style={{ fontSize: 20, color: '#08c' }} />
                    ABOUT ME
                </li>
                <li classNam="about" onClick={this.handleClickContact}style={{borderBottom:this.props.type2}}>
                    <Icon type="fork" style={{ fontSize: 20, color: '#08c' }} />
                    CONTACT
                </li>
                <li classNam="about" onClick={this.handleClickKnowledge} style={{borderBottom:this.props.type3}}>
                    <Icon type="bulb" style={{ fontSize: 20, color: '#08c' }} />
                    KNOWLEDGE
                </li>
            </ul>
        );
    }
}
var mapStateToProp = (state) => ({
});
var mapDispatchToProp = (dispatch,props) => ({
    pageAboutMe: () => dispatch(actions.pageAboutMe()),
    pageKnowledge: () => dispatch(actions.pageKnowledge()),
    pageContact: () => dispatch(actions.pageContact())
});
export default connect(mapStateToProp,mapDispatchToProp)(NavBar);
  