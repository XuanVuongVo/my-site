import React, { Component } from 'react';
import { Icon } from 'antd';
import '../../style/contact.css';

class ContactItem extends Component {
    render() {
        return (
            <li>
                <Icon type={this.props.type}/>
                <span>{this.props.content}</span>
            </li>
        );
    }
}
  
export default ContactItem;
  