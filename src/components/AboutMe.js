import React, { Component } from 'react';
import '../style/about-me.css';
import Target from './AboutMe/Target';
import Character from './AboutMe/Character';

class AboutMe extends Component {
    render() {
        return (
            <section className="sec-aboutme">
                <Target/>
                <Character/>
            </section>
        );
    }
}
  
export default AboutMe;
  