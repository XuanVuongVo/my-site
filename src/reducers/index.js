import { combineReducers } from 'redux';
import page from './page';

const allReducers = combineReducers({
    page
});

export default allReducers;