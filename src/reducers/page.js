import * as type from './../constants/ActionType';

var page = (state = '', action) => {
    switch(action.type) {
        case type.PAGE_ABOUT_ME:
            return type.PAGE_ABOUT_ME;
        case type.PAGE_KNOWLEDGE:
            return type.PAGE_KNOWLEDGE;
        case type.PAGE_CONTACT:
            return type.PAGE_CONTACT;
        default: 
            return state;
    }
};

export default page;